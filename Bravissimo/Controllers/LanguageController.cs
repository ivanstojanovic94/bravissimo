﻿using Bravissimo.Models;
using Bravissimo.Repositories;
using Bravissimo.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Bravissimo.Controllers
{
    public class LanguageController : Controller
    {
        private readonly ILanguageRepository _languageRepository;

        public LanguageController(ILanguageRepository languageRepository)
        {
            _languageRepository = languageRepository;
        }

        public IActionResult Index()
        {
            List<Language> objList = _languageRepository.GetLanguageList();
            
            if (objList == null)
                return NotFound();

            var vm = new LanguageViewModel() { LanguageList = objList };

            return View(vm);
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AddEditLanguageViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var obj = new Language()
                {
                    ID = vm.ID,
                    Name = vm.Name,
                    IsVisible = vm.IsVisible,
                    IsSrcAllowed = vm.IsSrcAllowed,
                    IsDstAllowed = vm.IsDstAllowed
                };

                try
                {
                    _languageRepository.AddLanguage(obj);
                }
                catch (Exception ex) 
                {
                    //ERROR ADDING LANGUAGE ERROR;
                }

                return RedirectToAction("Index");
            }
            return View(vm);
        }

        public IActionResult Edit(int? Id)
        {
            if (Id == null || Id == 0)
                return NotFound();

            var obj = _languageRepository.GetLanguageById(Id);

            if (obj == null)
                return NotFound();

            var vm = new AddEditLanguageViewModel()
            {
                ID = obj.ID,
                IsDstAllowed = obj.IsDstAllowed,
                IsSrcAllowed = obj.IsSrcAllowed,
                IsVisible = obj.IsVisible,
                Name = obj.Name
            };

            if (obj == null)
                return NotFound();

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(AddEditLanguageViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var obj = new Language()
                {
                    ID = vm.ID,
                    Name = vm.Name,
                    IsDstAllowed = vm.IsDstAllowed,
                    IsSrcAllowed = vm.IsSrcAllowed,
                    IsVisible = vm.IsVisible
                };

                try
                {
                    _languageRepository.UpdateLanguage(obj);
                }
                catch (Exception ex) 
                {
                    //ERROR UPDATE LANGUAGE;
                }

                return RedirectToAction("Index");
            }
            return View(vm);
        }

        public IActionResult Delete(int? Id)
        {
            if (Id == null || Id == 0)
                return NotFound();

            var obj = _languageRepository.GetLanguageById(Id);

            if (obj == null)
                return NotFound();

            var vm = new AddEditLanguageViewModel()
            {
                ID = obj.ID,
                Name = obj.Name,
                IsVisible = obj.IsVisible,
                IsSrcAllowed = obj.IsSrcAllowed,
                IsDstAllowed = obj.IsDstAllowed
            };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int? Id)
        {
            var obj = _languageRepository.GetLanguageById(Id);
            if (obj == null)
                return NotFound();

            try
            {
                _languageRepository.DeleteLanguage(obj);
            }
            catch (Exception ex) 
            {
                //ERROR DELETING LANGUAGE;
            }

            return RedirectToAction("Index");
        }
    }
}
