﻿using Bravissimo.Models;
using Bravissimo.Repositories;
using Bravissimo.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bravissimo.Controllers
{
    public class RequestController : Controller
    {
        private readonly IRequestRepository _requestRepository;
        private readonly ILanguageRepository _languageRepository;

        public RequestController( IRequestRepository requestRepository, ILanguageRepository languageRepository)
        { 
            _requestRepository = requestRepository;
            _languageRepository = languageRepository;
        }

        public IActionResult Index(string status)
        {
            if (status == null)
            {
                status = "primljeni";
            }

            List<Request> objList = _requestRepository.GetRequestList().Where(s => s.Status == status).ToList();

            if (objList == null)
                return NotFound();
 
            var vm = new RequestViewModel() { RequestList = objList };
            vm.status = status;
            return View(vm);
        }

        public IActionResult Edit(int? Id)
        {
            if (Id == null || Id == 0)
                return NotFound();

            var obj = _requestRepository.GetRequestById(Id);
            var languages = _languageRepository.GetLanguageList();

            if (obj == null)
                return NotFound();

            var vm = new EditRequestViewModel()
            {
                ID = obj.ID,
                ReceivedDate = obj.ReceivedDate,
                SentBy = obj.SentBy,
                SrcLangId = obj.SrcLang.ID,
                DstLangId = obj.DstLang.ID,
                SrcLang = obj.SrcLang,
                DstLang = obj.DstLang,
                Deadline = obj.Deadline,
                ResponseDate = obj.ResponseDate,
                RespondedBy = obj.RespondedBy,
                ResponseMessage = obj.ResponseMessage,
                Price = obj.Price,
                Language = languages
            };

            if (obj == null)
                return NotFound();

            return View(vm);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditRequestViewModel vm)
        {

            if (ModelState.IsValid)
            {
                var obj = new Request()
                {
                    ID = vm.ID,
                    ReceivedDate = vm.ReceivedDate,
                    SentBy = vm.SentBy,
                    Deadline = vm.Deadline,
                    ResponseDate = vm.ResponseDate,
                    RespondedBy = vm.RespondedBy,
                    ResponseMessage = vm.ResponseMessage,
                    Price = vm.Price,
                    Status = "odgovoreni"
                };

                obj.DstLang = _languageRepository.GetLanguageById(vm.DstLangId);
                obj.SrcLang = _languageRepository.GetLanguageById(vm.SrcLangId);

                try
                {
                    _requestRepository.UpdateRequest(obj);
                }
                catch (Exception ex)
                {
                    //ERROR UPDATE LANGUAGE;
                }

                return RedirectToAction("Index");
            }
            return View(vm);
        }
        public IActionResult Delete(int? Id)
        {
            if (Id == null || Id == 0)
                return NotFound();

            var obj = _requestRepository.GetRequestById(Id);

            if (obj == null)
                return NotFound();

            var vm = new EditRequestViewModel()
            {
                ID = obj.ID,
                ReceivedDate = obj.ReceivedDate,
                SentBy = obj.SentBy,
                SrcLang = obj.SrcLang,
                DstLang = obj.DstLang,
                Deadline = obj.Deadline,
                ResponseDate = obj.ResponseDate,
                RespondedBy = obj.RespondedBy,
                ResponseMessage = obj.ResponseMessage,
                Price = obj.Price
            };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int? Id)
        {
            var obj = _requestRepository.GetRequestById(Id);
            if (obj == null)
                return NotFound();

            try
            {
                _requestRepository.DeleteRequest(obj);
            }
            catch (Exception ex)
            {
                //ERROR DELETING LANGUAGE;
            }

            return RedirectToAction("Index");
        }

        //[HttpPost]
        //public ActionResult FilterByStatus (string status)
        //{
        //    return RedirectToAction("Index", new { status = status});
        //}
    }
}
