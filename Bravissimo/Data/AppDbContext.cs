﻿using Bravissimo.Models;
using Microsoft.EntityFrameworkCore;

namespace Bravissimo.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Language> Languages { get; set; }
        public DbSet<Request> Requests { get; set; }
    }
}
