﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bravissimo.Models
{
    public class Language
    {
        [Key]
        public virtual int ID { get; set; }
        [Required, DisplayName("Language")]
        public virtual string Name { get; set; }
        [Required, DisplayName("Is Visible")]
        public virtual bool IsVisible { get; set; }
        [Required, DisplayName("Is Src Allowed")]
        public virtual bool IsSrcAllowed { get; set; }
        [Required, DisplayName("Is Dst Allowed")]
        public virtual bool IsDstAllowed { get; set; }
    }
}
