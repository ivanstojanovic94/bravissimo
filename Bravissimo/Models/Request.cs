﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bravissimo.Models
{
    public class Request
    {
        [Key]
        public virtual int ID { get; set; }
        [Required, DisplayName("Received Date")]
        public virtual DateTime ReceivedDate { get; set; }
        [Required, DisplayName("Send By")]
        public virtual string SentBy { get; set; }
        [Required, ForeignKey("SrcLangId")]
        public virtual Language SrcLang { get; set; }
        [Required, ForeignKey("DstLangId")]
        public virtual Language DstLang { get; set; }
        [Required, DisplayName("Deadline")]
        public virtual DateTime Deadline { get; set; }
        [DisplayName("Response Date")]
        public virtual DateTime ResponseDate { get; set; }
        [DisplayName("Responded By")]
        public virtual string RespondedBy { get; set; }
        [DisplayName("Response Message")]
        public virtual string ResponseMessage { get; set; }
        [Required, DisplayName("Price")]
        public virtual string Price { get; set; }
        [DisplayName("Status")]
        public virtual string Status { get; set; }

    }
}
