﻿using Bravissimo.Models;
using System.Collections.Generic;

namespace Bravissimo.Repositories
{
    public interface ILanguageRepository
    {
        public List<Language> GetLanguageList();
        void AddLanguage(Language obj);
        Language GetLanguageById(int? id);
        void UpdateLanguage(Language obj);
        void DeleteLanguage(Language obj);
    }
}
