﻿using Bravissimo.Models;
using System.Collections.Generic;

namespace Bravissimo.Repositories
{
    public interface IRequestRepository
    {
        List<Request> GetRequestList();
        List<Request> GetRequestListByStatus(string status);
        Request GetRequestById(int? id);
        void UpdateRequest(Request obj);
        void DeleteRequest(Request obj);
    }
}