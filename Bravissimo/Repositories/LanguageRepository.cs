﻿using Bravissimo.Data;
using Bravissimo.Models;
using System.Collections.Generic;
using System.Linq;

namespace Bravissimo.Repositories
{
    public class LanguageRepository : ILanguageRepository
    {
        private readonly AppDbContext _db;

        public LanguageRepository(AppDbContext db)
        {
            _db = db;
        }

        public List<Language> GetLanguageList() 
        {
            return _db.Languages.ToList();
        }

        public void AddLanguage(Language obj) 
        {
            _db.Languages.Add(obj);
            _db.SaveChanges();
        }

        public Language GetLanguageById(int? id) 
        {
            return _db.Languages.Find(id);
        }

        public void UpdateLanguage(Language obj) 
        {
            _db.Languages.Update(obj);
            _db.SaveChanges();
        }

        public void DeleteLanguage(Language obj) 
        {
            _db.Languages.Remove(obj);
            _db.SaveChanges();
        }
    }
}
