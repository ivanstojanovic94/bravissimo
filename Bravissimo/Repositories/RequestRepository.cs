﻿using Bravissimo.Data;
using Bravissimo.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Bravissimo.Repositories
{
    public class RequestRepository : IRequestRepository
    {
        private readonly AppDbContext _db;

        public RequestRepository(AppDbContext db)
        {
            _db = db;
        }

        public List<Request> GetRequestList()
        {
            return _db.Requests.Include(x => x.DstLang).Include(x => x.SrcLang).ToList();
        }

        public List<Request> GetRequestListByStatus(string status)
        {
            return _db.Requests.Where(s => s.Status == status ).ToList();
        }

        public void UpdateRequest(Request obj)
        {
            _db.Requests.Update(obj);
            _db.SaveChanges();
        }
        public void DeleteRequest(Request obj)
        {
            _db.Requests.Remove(obj);
            _db.SaveChanges();
        }

        Request IRequestRepository.GetRequestById(int? id)
        {
            return _db.Requests.Find(id);
        }

    }
}
