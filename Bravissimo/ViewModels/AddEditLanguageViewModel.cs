﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Bravissimo.ViewModels
{
    public class AddEditLanguageViewModel
    {
        public int ID { get; set; }
        [Required, DisplayName("Language")]
        public string Name { get; set; }
        [Required, DisplayName("Is Visible")]
        public bool IsVisible { get; set; }
        [Required, DisplayName("Is Src Allowed")]
        public bool IsSrcAllowed { get; set; }
        [Required, DisplayName("Is Dst Allowed")]
        public bool IsDstAllowed { get; set; }
    }
}
