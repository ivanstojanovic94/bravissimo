﻿using Bravissimo.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bravissimo.ViewModels
{
    public class EditRequestViewModel
    {
        public  int ID { get; set; }
        [Required, DisplayName("Received Date")]
        public DateTime ReceivedDate { get; set; }
        [Required, DisplayName("Send By")]
        public  string SentBy { get; set; }
        [ForeignKey("SrcLangId"), DisplayName("Source Language")]
        public  Language SrcLang { get; set; }
        public int SrcLangId { get; set; }
        [ForeignKey("DstLangId"), DisplayName("Destination Language")]
        public Language DstLang { get; set; }
        public int DstLangId { get; set; }
        [Required, DisplayName("Deadline")]
        public  DateTime Deadline { get; set; }
        [DisplayName("Response Date")]
        public  DateTime ResponseDate { get; set; }
        [DisplayName("Responded By")]
        public  string RespondedBy { get; set; }
        [DisplayName("Response Message")]
        public  string ResponseMessage { get; set; }
        [Required, DisplayName("Price")]
        public  string Price { get; set; }
        [DisplayName("Status")]
        public  string Status { get; set; }
        public  List<Language> Language { get; set; }
    




    }
}
