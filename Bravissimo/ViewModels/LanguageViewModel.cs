﻿using Bravissimo.Models;
using System.Collections.Generic;

namespace Bravissimo.ViewModels
{
    public class LanguageViewModel
    {
        public List<Language> LanguageList { get; set; }
    }
}
