﻿using Bravissimo.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bravissimo.ViewModels
{
    public class RequestViewModel
    {
        public List<Request> RequestList { get; set; }

        public string Status { get; set; }

        public string ReceivedSort = "primljeni";

        public string AnsweredSort = "odgovoreni";
    }
}